//
//  File.swift
//  
//
//  Created by Vignesh Sankaran on 25/4/20.
//

import Foundation
import PTVSwift

struct StopsForRoute {
    let stops: V3Stops
    let routeId: Int
}
