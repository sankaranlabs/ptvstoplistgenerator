//
//  File.swift
//  
//
//  Created by Vignesh Sankaran on 23/11/19.
//

import Foundation

struct StopDetails: Encodable {
    let stops: [StopDetail]
}

struct StopDetail: Encodable, Hashable {
    let id: Int
    let name: String
    var routeId: [Int]
}
