import Foundation
import Combine
import PTVSwift

var p: AnyPublisher<V3Routes, Error>?
var cancellable = Set<AnyCancellable>()
let semaphore = DispatchSemaphore(value: 0)
var routes: V3Routes?
var stops = [StopsForRoute]()
var stopDetails = [StopDetails]()

func configure() {
    guard let devId = ProcessInfo.processInfo.environment["DEV_ID"] else {
        fatalError("DEV_ID is not set as an environment variable!")
    }
    
    guard let securityKey = ProcessInfo.processInfo.environment["SECURITY_KEY"] else {
        fatalError("SECURITY_KEY is not set as environment variable!")
    }
    
    Configuration.devId = devId
    Configuration.securityKey = securityKey
}

func fetchRoutes() {
    let routesResult = Routes().getAllRoutes(routeTypes: [0])

    guard let routesPublisher = try? routesResult.get() else {
        fatalError("Failed to create a Routes publisher!")
    }

    routesPublisher.sink(receiveCompletion: { routesError in
        if case .failure(let error) = routesError {
            fatalError("Failed to get routes with error: \(error)")
        }
    }, receiveValue: { values in
        routes = values
        semaphore.signal()
    })
        .store(in: &cancellable)
    
    semaphore.wait()
    
    // Insert direction IDs for each stop on each route

    // Query each route for all directions available

    let stopsOnRoutes: [AnyPublisher<StopsForRoute, Error>] = routes!.routes.compactMap { route in
        let stopsResult = Stops().getStopsByRouteId(routeId: route.routeID, routeType: route.routeType)
        
        guard let stopsPublisher = try? stopsResult.get() else {
            fatalError("Failed to create a Stops publisher!")
        }
        
       return stopsPublisher
        .map { StopsForRoute(stops: $0 as V3Stops, routeId: route.routeID) }
        .eraseToAnyPublisher()
    }
    
    Publishers.Sequence(sequence: stopsOnRoutes)
        .flatMap { $0 }
        .collect()
        .eraseToAnyPublisher()
    .sink(receiveCompletion: { _ in }, receiveValue: { values in
        stops = values
        semaphore.signal()
    })
    .store(in: &cancellable)
    
    semaphore.wait()
    
    var stopList = [Int: (String, [Int])]()
    
    // Flatten the data structure
    _ = stops.flatMap { stopArray in
        stopArray.stops.stops.flatMap { stop in
            if stopList[stop.stopID] == nil {
                stopList[stop.stopID] = (stop.stopName, [stopArray.routeId])
            } else {
                stopList[stop.stopID]?.1.append(stopArray.routeId)
            }
        }
    }
    
    var stopDetails = stopList.map { index, key in
        StopDetail(id: index, name: key.0, routeId: key.1.sorted())
    }
    
    stopDetails.sort { $0.name < $1.name }
    
    let array = StopDetails(stops: stopDetails)
    
    let json = try! JSONEncoder().encode(array)
    let jsonString = String(bytes: json, encoding: .utf8)

    // Retrieved from https://www.hackingwithswift.com/example-code/strings/how-to-save-a-string-to-a-file-on-disk-with-writeto on 23/11/2019
    let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("MetroStops.json")
    
    do {
        try jsonString!.write(to: path, atomically: true, encoding: .utf8)
    } catch {
        fatalError("Failed to write to file with error: \(error)")
    }
}

func getRouteData(for url: URL) -> AnyPublisher<V3Routes, Error> {
    return URLSession
        .shared
        .dataTaskPublisher(for: url)
        .map { $0.data }
        .decode(type: V3Routes.self, decoder: JSONDecoder())
        .eraseToAnyPublisher()
}

func getStopsData(for url: URL) -> AnyPublisher<V3Stops, Error> {
    return URLSession
        .shared
        .dataTaskPublisher(for: url)
        .map { $0.data }
        .decode(type: V3Stops.self, decoder: JSONDecoder())
        .eraseToAnyPublisher()
}

configure()
fetchRoutes()
