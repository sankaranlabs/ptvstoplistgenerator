// swift-tools-version:5.1
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "PTVStopListGenerator",
    platforms: [
        .macOS(.v10_15),
    ],
    dependencies: [
         .package(url: "https://github.com/vignesh-sankaran/PTVSwift.git", from: "0.0.6"),
    ],
    targets: [
        .target(
            name: "PTVStopListGenerator",
            dependencies: ["PTVSwift"]),
        .testTarget(
            name: "PTVStopListGeneratorTests",
            dependencies: ["PTVStopListGenerator"]),
    ]
)
