import XCTest

import PTVStopListGeneratorTests

var tests = [XCTestCaseEntry]()
tests += PTVStopListGeneratorTests.allTests()
XCTMain(tests)
